from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'card.views.index', name="index"),
    url(r'^confirmar/$', 'card.views.confirm', name="data-confirm"),
)