# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Vehicle'
        db.create_table('card_vehicle', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('license_plate', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('transit_entity', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('vehicles_number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal('card', ['Vehicle'])


    def backwards(self, orm):
        # Deleting model 'Vehicle'
        db.delete_table('card_vehicle')


    models = {
        'card.card': {
            'Meta': {'object_name': 'Card'},
            'card_kind': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['card.Person']", 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'card.person': {
            'Meta': {'object_name': 'Person'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'birth_day': ('django.db.models.fields.DateField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'document': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_death': ('django.db.models.fields.CharField', [], {'default': "'2'", 'max_length': '1'}),
            'kind_document': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'last_name_second': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'license_plate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'locality': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'neighborhood': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'record_day': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'record_month': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'record_year': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'sdm': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'transit_entity': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'vehicles_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'card.vehicle': {
            'Meta': {'object_name': 'Vehicle'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'license_plate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'transit_entity': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'vehicles_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['card']