# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Card'
        db.create_table('card_card', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('card_kind', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('person', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['card.Person'], unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('card', ['Card'])

        # Adding model 'Person'
        db.create_table('card_person', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('record_day', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('record_month', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('record_year', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('second_name', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('last_name_second', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('kind_document', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('document', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('birth_day', self.gf('django.db.models.fields.DateField')()),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('locality', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('neighborhood', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('is_death', self.gf('django.db.models.fields.CharField')(default='2', max_length=1)),
            ('sdm', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('license_plate', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('transit_entity', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('vehicles_number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('card', ['Person'])


    def backwards(self, orm):
        # Deleting model 'Card'
        db.delete_table('card_card')

        # Deleting model 'Person'
        db.delete_table('card_person')


    models = {
        'card.card': {
            'Meta': {'object_name': 'Card'},
            'card_kind': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['card.Person']", 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'card.person': {
            'Meta': {'object_name': 'Person'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'birth_day': ('django.db.models.fields.DateField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'document': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_death': ('django.db.models.fields.CharField', [], {'default': "'2'", 'max_length': '1'}),
            'kind_document': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'last_name_second': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'license_plate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'locality': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'neighborhood': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'record_day': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'record_month': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'record_year': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'sdm': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'transit_entity': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'vehicles_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['card']