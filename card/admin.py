from django.contrib import admin

from card.models import Card, Person, Vehicle, Novelty

class CardAdmin(admin.ModelAdmin):
    pass

class NoveltyInline(admin.TabularInline):
    model = Novelty        

class PersonAdmin(admin.ModelAdmin):
    list_display = (
        # 'record_day',
        # 'record_month',
        # 'record_year',
        'first_name',
        'second_name',
        'last_name',
        'last_name_second',
        'kind_document',
        'document',
        'birth_day',
        # 'address',
        'locality',
        # 'neighborhood',
        'is_death',
    )
    list_filter = (
        # 'record_day',
        # 'record_month',
        # 'record_year',
        'kind_document',
        'locality',
        'is_death',
    )
    list_display_links = ('document',)
    ordering = ('last_name', 'last_name_second', 'last_name', 'last_name_second')
    search_fields = ('first_name', 'second_name', 'last_name', 'last_name_second', 'document')
    inlines = (NoveltyInline,)

class VehicleAdmin(admin.ModelAdmin):
    list_display = ('license_plate', 'transit_entity')
    ordering = ('license_plate',)
    search_fields = ('license_plate',)

admin.site.register(Card, CardAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(Vehicle, VehicleAdmin)
