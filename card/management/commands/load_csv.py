# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import csv
import itertools


from dateutil.parser import parse

from django.core.management.base import BaseCommand, CommandError
from card.models import Person, Vehicle

def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    fields = csv_reader.next()
    fields_utf8 = [unicode(cell, 'iso-8859-1') for cell in fields]
    for row in csv_reader:    
        yield dict(itertools.izip(fields_utf8,(unicode(cell, 'iso-8859-1') for cell in row)))

class Command(BaseCommand):
    can_import_settings = True
    help = 'Load database from csv'
    requires_model_validation = True
    args = 'file.csv'

    def handle(self, *args, **options):
        csv_file_path = args[0]
        csv_file = open(csv_file_path, "r")
        line_counter = 0
        for line in unicode_csv_reader(csv_file, delimiter=';'):
            try:
                qs = Person.objects.all().filter(document=line['numero_documento'], kind_document=line['id_tipo_doc_ind'])
                if qs.exists():
                    person = qs[0]
                    print(".", end='')
                else:
                    person = Person(document=line['numero_documento'], kind_document=line['id_tipo_doc_ind'])
                    print('+', end='')
                person.record_day = int(line['Dia'] or 0)
                person.record_month = int(line['Mes'] or 0)
                person.record_year = int(float(line[u'Año'].replace(",", ".") or 0))
                person.first_name = line['primer_nombre']
                person.second_name = line['segundo_nombre']
                person.last_name = line['primer_apellido']
                person.last_name_second = line['segundo_apellido']
                person.birth_day = parse(line['fecha_nacimiento'])
                person.address = line['direccion']
                person.locality = line['id_localidad_ind'] or 0
                person.neighborhood = line['barrio']
                person.is_death = line['fallecido']
                person.sdm = line['Id_Novedad']
                person.save()
                vehicle, created = Vehicle.objects.get_or_create(license_plate=line['Placa'])
                vehicle.transit_entity = line['OT']
                vehicle.save()
                person.vehicles.add(vehicle)
            except Exception, error:
                print(error)
                print(line)
            line_counter += 1
        print("Cargados {0} registros".format(line_counter))
        csv_file.close()