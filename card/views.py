from django.shortcuts import render, redirect

from appointment.forms import AppointmentStep1
from card.forms import SearchForm, ConfirmationForm
from card.models import Person

def user_have_card(person):
    import MySQLdb
    KINDS_MAP = {
        "CC": 151,
        "CE": 153,
        "PA": 156,
        "RC": 155,
        "TI": 154,
    }
    connection_params = {
        'host': "sisu-db.cfuphjwmez2w.us-east-1.rds.amazonaws.com",
        #'host': "portalweb-db.cr0k1tnuhdai.us-west-2.rds.amazonaws.com",
        'user': "sisudb",
        'passwd': "SXuWTiiNKX67kXP",
        'db': "sisudb",
    }
    if person.kind_document in KINDS_MAP:
        db = MySQLdb.connect(**connection_params)
        query_str = """
        select PrmTipoIdentificacion, Identificacion, PrmPoblacionEspecial 
        from PtrSolicitudesPersonalizacion
        where PrmTipoIdentificacion={kind_document} and Identificacion={document}
        LIMIT 1
        """.format(kind_document=KINDS_MAP[person.kind_document], document=person.document)
        db.query(query_str)
        result = db.store_result()
        for row in result.fetch_row(how=1):
            if row['PrmPoblacionEspecial'] == 1359:
                return True, True
            return True, False
    return False, False

def index(request):
    context = {}
    MSG_NOBD = False # No esta en la BD
    MSG_PYP = False # Pico y placa
    MSG_VHCL = False # propietario
    MSG_CHC = False
    MSG_DTH = False # fallecido
    
    if request.method == "POST":
        search_form = SearchForm(request.POST)
        if search_form.is_valid():
            kd = search_form.cleaned_data['kind_document']
            d = search_form.cleaned_data['document']
            qs = Person.objects.all().filter(kind_document=kd, document=d)
            if qs.exists():
                person = qs[0]
                context['person'] = person
                # context['show_person_data'] = True
                
                if person.novelties.filter(sdm="1").exists():
                    MSG_PYP = True
                    search_form = None
                elif person.novelties.filter(sdm="2").exists():
                    search_form = None
                    MSG_VHCL = True
                    plates = [v.license_plate for v in person.vehicles.all()]
                    vehicles_count = person.vehicles.all().count()
                    plates = list(set(plates))
                    vehicles_count=len(plates)
                    context['plates'] = plates
                    context['vehicles_count'] = vehicles_count
                elif person.novelties.filter(sdm="3").exists():
                    search_form = None
                    MSG_CHC = True
                elif person.is_death == "2":
                    search_form = None
                    MSG_DTH = True
                else:
                    context['person'] = person
                    context['show_person_data'] = True
                    confirmation_form = ConfirmationForm(
                            initial = {
                                    'person_id': person.id,
                                    'person_document': person.document,
                                    'confirm' : True,
                                }
                        )
                    context['confirmation_form'] = confirmation_form
            else:
                search_form = None
                MSG_NOBD = True
    else:
        search_form = SearchForm()

    context['search_form'] = search_form
    context['MSG_NOBD'] = MSG_NOBD
    context['MSG_PYP'] = MSG_PYP
    context['MSG_VHCL'] = MSG_VHCL
    context['MSG_CHC'] = MSG_CHC
    context['MSG_DTH'] = MSG_DTH
    return render(request, "card/index.html", context)

def confirm(request):
    MSG_ID = False # Mensaje de datos incorrectos
    context = {}
    if request.method == "POST":
        confirmation_form = ConfirmationForm(request.POST)
        if confirmation_form.is_valid():
            person_id = confirmation_form.cleaned_data['person_id']
            person_document = confirmation_form.cleaned_data['person_document']
            confirm = confirmation_form.cleaned_data['confirm']
            if confirm:
                #person_qs = Person.objects.all().filter(id=person_id, document=person_document)
                #if person_qs.exists():
                    #have_card, is_senior_card = user_have_card(person_qs[0])
                    context['have_card'] = 'have_card'
                    context['is_senior_card'] = 'have_card'
                    appointment_step1_form = AppointmentStep1(initial=request.POST)
                    context['appointment_step1_form'] = appointment_step1_form
                    return render(request, "appointment/schedule.html", context)
                #else:
                    # El formulario de confirmacion ha sido jaqueado
                #    return redirect('index')
            else:
                MSG_ID = True
                context['MSG_ID'] = MSG_ID
                return render(request, "card/index.html", context)
        else:
            return redirect('index')
    else:
        return redirect('index')

def root_index(request):
    return redirect('index')
