SELECT 
        count(appointment_appointment.id) AS Cantidad, 
        DAY(appointment_appointment.appointment_date) AS day,
        appointment_place.name as Estacion
    FROM appointment_appointment, appointment_place
    WHERE
            YEAR(appointment_appointment.appointment_date) = {anio}
            AND 
            MONTH(appointment_appointment.appointment_date) = {mes}
            AND 
            appointment_place.id=appointment_appointment.rendezvous_id
    GROUP BY DAY(appointment_appointment.appointment_date), appointment_appointment.rendezvous_id;