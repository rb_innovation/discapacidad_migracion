from django.db import models

DAYS_CHOICES = (
    (0, "Lunes"),
    (1, "Martes"),
    (2, "Miercoles"),
    (3, "Jueves"),
    (4, "Viernes"),
    (5, "Sabado"),
    (6, "Domingo"),
    (7, "Festivo"),
)

class Timetable(models.Model):
    place = models.ForeignKey('Place', related_name='timetables')
    day = models.PositiveSmallIntegerField(choices=DAYS_CHOICES)
    capacity = models.PositiveSmallIntegerField(default=3)
    since = models.TimeField()
    until = models.TimeField()

    class Meta:
        unique_together = (('place','day'),)

    def __unicode__(self):
        day = self.get_day_display()
        return u"{day}, {since} - {until}".format(day=day, since=self.since, until=self.until)

class Place(models.Model):
    name = models.CharField(max_length=300)
    address = models.CharField(max_length=300)
    time_slot = models.PositiveSmallIntegerField(default=15, help_text="In minutes")
    active = models.IntegerField()

    def __unicode__(self):
        return u"{0.name} ({0.address})".format(self)

class Appointment(models.Model):
    appointment_date = models.DateTimeField()
    rendezvous = models.ForeignKey("Place", related_name="appointments")
    person = models.ForeignKey("card.Person")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        unique_together = (('appointment_date','rendezvous', 'person'),)
        ordering = ('rendezvous', 'appointment_date', )