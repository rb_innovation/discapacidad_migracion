from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^disponibilidad/', 'appointment.views.appointment_step2', name="appointment-step2"),
    url(r'^asignar/(?P<place_url>\d+)/(?P<day_url>\d{8})', 'appointment.views.appointment_step3', name="appointment-step3"),
)