from dateutil.parser import parse

from django.shortcuts import render, redirect

from appointment.forms import AppointmentStep1, AppointmentStep2
from appointment.models import Appointment

def appointment_step2(request):
    context = {}
    if request.method == "POST":
        appointment_step1_form = AppointmentStep1(request.POST)
        if appointment_step1_form.is_valid():
            place_id = appointment_step1_form.cleaned_data['place_id']
            day = appointment_step1_form.cleaned_data['day']
            appointment_step2_form = AppointmentStep2(place_id=place_id, day=day, initial=request.POST)
            context['appointment_step2_form'] = appointment_step2_form
            context['place_url'] = place_id
            context['day_url'] = day
        else:
            return redirect('index')
    else:
        return redirect('index')
    return render(request, "appointment/schedule.html", context)

def appointment_step3(request, place_url, day_url):
    context = {}
    if request.method == "POST":
        appointment_step2_form = AppointmentStep2(request.POST, place_id=place_url, day=day_url)
        if appointment_step2_form.is_valid():
            person_id = appointment_step2_form.cleaned_data['person_id']
            person_document = appointment_step2_form.cleaned_data['person_document']
            rendezvous_id = appointment_step2_form.cleaned_data['place_id']
            day = appointment_step2_form.cleaned_data['day']
            time = appointment_step2_form.cleaned_data['time']
            appointment = Appointment()
            appointment.appointment_date = parse(time)
            appointment.rendezvous_id = rendezvous_id
            appointment.person_id = person_id
            try:
                appointment.save()
                context['done'] = True
                context['appointment_date'] = appointment.appointment_date
                context['rendezvous'] = appointment.rendezvous
                return render(request, "appointment/schedule.html", context)
            except Exception, error:
                print error
                context['error_on_schedule'] = True
                return render(request, "appointment/schedule.html", context)
        else:
            context['appointment_step2_form'] = appointment_step2_form
            context['place_url'] = place_url
            context['day_url'] = day_url
            return render(request, "appointment/schedule.html", context)
    else:
        return redirect('index')