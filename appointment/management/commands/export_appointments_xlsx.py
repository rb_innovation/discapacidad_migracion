# -*- coding: utf-8 -*-
from datetime import datetime
from itertools import izip

from openpyxl import Workbook
from openpyxl.cell import get_column_letter

from django.core.mail import EmailMessage
from django.core.management.base import NoArgsCommand, CommandError
from django.utils import timezone

from appointment.models import Appointment, Place

class Command(NoArgsCommand):
    can_import_settings = True
    help = 'Send files with appointments'
    requires_model_validation = True

    def handle_noargs(self, *args, **kwargs):

        workbook = Workbook()

        today = datetime.today()

        dest_file = r'/tmp/Citas Decreto 429 {0}.xlsx'.format(today.strftime("%Y-%m-%d"))

        first_sheet = True

        kwargs = {
            'appointments__appointment_date__year' : today.year,
            'appointments__appointment_date__month' : today.month,
            'appointments__appointment_date__day' : today.day,
        }

        for place in Place.objects.all().filter(**kwargs).distinct():

            if first_sheet:
                place_worksheet = workbook.worksheets[0]
                first_sheet = False
            else:
                place_worksheet = workbook.create_sheet()
            
            place_worksheet.title = place.name

            # print u"writing worksheet: {0}".format(place.id)

            # Write the first row
            headers = [u'Fecha', u'Tipo de Documento', u'Documento', u'Primer Nombre', u'Segundo Nombre', u'Primer Apellido', u'Segundo Apellido']

            # print u"writing headers"

            for col_counter, header in izip(xrange(1,len(headers)+1), headers):
                col = get_column_letter(col_counter)
                place_worksheet.cell("{0}1".format(col)).value = header

            appointments_qs = Appointment.objects.all()
            appointments_qs = appointments_qs.filter(appointment_date__year=today.year)
            appointments_qs = appointments_qs.filter(appointment_date__month=today.month)
            appointments_qs = appointments_qs.filter(appointment_date__day=today.day)
            appointments_qs = appointments_qs.filter(rendezvous=place)
            appointments_qs = appointments_qs.order_by('appointment_date')

            for row_counter, appointment in izip(range(2, appointments_qs.count()+2), appointments_qs):
                # print "writing row {0}".format(row_counter)
                place_worksheet.cell("{0}{1}".format(get_column_letter(1), row_counter)).value = appointment.appointment_date.astimezone(timezone.get_default_timezone()).time()
                place_worksheet.cell("{0}{1}".format(get_column_letter(2), row_counter)).value = appointment.person.get_kind_document_display()
                place_worksheet.cell("{0}{1}".format(get_column_letter(3), row_counter)).value = appointment.person.document
                place_worksheet.cell("{0}{1}".format(get_column_letter(4), row_counter)).value = appointment.person.first_name
                place_worksheet.cell("{0}{1}".format(get_column_letter(5), row_counter)).value = appointment.person.second_name
                place_worksheet.cell("{0}{1}".format(get_column_letter(6), row_counter)).value = appointment.person.last_name
                place_worksheet.cell("{0}{1}".format(get_column_letter(7), row_counter)).value = appointment.person.last_name_second

        workbook.save(filename = dest_file)

        # send email
        email_options = {
            'subject': 'Citas Decreto 429 {0}.xlsx'.format(today.strftime("%Y-%m-%d")),
            'body': "Mensaje creado automaticamente con el listado de citas",
            'from_email': "dsanabria@rbsas.co",
            'to': [
                'decreto429@rbsas.co',
            ],
            'bcc': [
                'dsanabria@rbsas.co',
                'jcruz@rbsas.co',
            ],
            'headers': {
                'Reply-To': 'dsanabria@rbsas.co',
            },
        } 
        email = EmailMessage(**email_options)
        email.attach_file(dest_file)
        email.send(fail_silently=True)