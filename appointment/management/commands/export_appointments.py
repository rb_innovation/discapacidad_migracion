# -*- coding: utf-8 -*-
import codecs
import os
from datetime import datetime

from django.core.management.base import NoArgsCommand, CommandError
from django.template.defaultfilters import slugify
from django.utils import timezone

from appointment.models import Appointment, Place

def write_appointment(csv_file, appointment):
    cells = []
    cells.append(appointment.appointment_date.astimezone(timezone.get_default_timezone()).strftime('"%Y/%m/%d %I:%M %p"'))
    cells.append(u'"{0}"'.format(appointment.person.get_kind_document_display()))
    cells.append(u'"{0}"'.format(appointment.person.document))
    cells.append(u'"{0}"'.format(appointment.person.first_name))
    cells.append(u'"{0}"'.format(appointment.person.second_name))
    cells.append(u'"{0}"'.format(appointment.person.last_name))
    cells.append(u'"{0}"'.format(appointment.person.last_name_second))
    cells.append(u'\n')
    csv_file.write(u','.join(cells))

class Command(NoArgsCommand):
    can_import_settings = True
    help = 'Send files with appointments'
    requires_model_validation = True

    def handle_noargs(self, *args, **kwargs):
        today = datetime.today()
        today = datetime(2012, 10, 29)
        for place in Place.objects.all().filter(appointments__appointment_date__day=today.day).distinct():
            # file_name = slugify(u"{0.name}.csv".format(place))
            file_name = "{0}.{1}.csv".format('-'.join([str(today.year), str(today.month), str(today.day)]), slugify(place.name))
            with codecs.open(os.sep.join(["", "Users", "diegueus9", file_name]), "wb", "utf-8-sig") as csv_file:
                csv_file.write(u'Fecha,Tipo de Documento, Documento, Primer Nombre, Segundo Nombre, Primer Apellido, Segundo Apellido\n')
                appointments_qs = Appointment.objects.all()
                appointments_qs = appointments_qs.filter(appointment_date__day=today.day)
                appointments_qs = appointments_qs.filter(rendezvous=place)
                # appointments_qs = appointments_qs.values('appointment_date', 'person__kind_document', 'person__document', 'person__first_name', 'person__second_name', 'person__last_name', 'person__last_name_second')
                appointments_qs = appointments_qs.order_by('appointment_date')
                print appointments_qs.query
                for appointment in appointments_qs:
                    write_appointment(csv_file, appointment)
                # csv_file.close()
