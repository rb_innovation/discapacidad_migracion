# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Timetable'
        db.create_table('appointment_timetable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('day', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('capacity', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=3)),
            ('since', self.gf('django.db.models.fields.TimeField')()),
            ('until', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal('appointment', ['Timetable'])

        # Adding model 'Place'
        db.create_table('appointment_place', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('time_slot', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=15)),
        ))
        db.send_create_signal('appointment', ['Place'])

        # Adding M2M table for field timetables on 'Place'
        db.create_table('appointment_place_timetables', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('place', models.ForeignKey(orm['appointment.place'], null=False)),
            ('timetable', models.ForeignKey(orm['appointment.timetable'], null=False))
        ))
        db.create_unique('appointment_place_timetables', ['place_id', 'timetable_id'])

        # Adding model 'Appointment'
        db.create_table('appointment_appointment', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('appointment_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('rendezvous', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appointment.Place'])),
            ('person', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['card.Person'])),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('deleted_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal('appointment', ['Appointment'])


    def backwards(self, orm):
        # Deleting model 'Timetable'
        db.delete_table('appointment_timetable')

        # Deleting model 'Place'
        db.delete_table('appointment_place')

        # Removing M2M table for field timetables on 'Place'
        db.delete_table('appointment_place_timetables')

        # Deleting model 'Appointment'
        db.delete_table('appointment_appointment')


    models = {
        'appointment.appointment': {
            'Meta': {'object_name': 'Appointment'},
            'appointment_date': ('django.db.models.fields.DateTimeField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'deleted_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['card.Person']"}),
            'rendezvous': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['appointment.Place']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'appointment.place': {
            'Meta': {'object_name': 'Place'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'time_slot': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '15'}),
            'timetables': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['appointment.Timetable']", 'symmetrical': 'False'})
        },
        'appointment.timetable': {
            'Meta': {'object_name': 'Timetable'},
            'capacity': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'}),
            'day': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'since': ('django.db.models.fields.TimeField', [], {}),
            'until': ('django.db.models.fields.TimeField', [], {})
        },
        'card.person': {
            'Meta': {'object_name': 'Person'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'birth_day': ('django.db.models.fields.DateField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'document': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_death': ('django.db.models.fields.CharField', [], {'default': "'2'", 'max_length': '1'}),
            'kind_document': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'last_name_second': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'license_plate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'locality': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'neighborhood': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'record_day': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'record_month': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'record_year': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'sdm': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'transit_entity': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'vehicles_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['appointment']