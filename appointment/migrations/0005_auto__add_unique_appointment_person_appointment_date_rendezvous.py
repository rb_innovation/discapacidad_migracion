# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Appointment', fields ['person', 'appointment_date', 'rendezvous']
        db.create_unique('appointment_appointment', ['person_id', 'appointment_date', 'rendezvous_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Appointment', fields ['person', 'appointment_date', 'rendezvous']
        db.delete_unique('appointment_appointment', ['person_id', 'appointment_date', 'rendezvous_id'])


    models = {
        'appointment.appointment': {
            'Meta': {'unique_together': "(('appointment_date', 'rendezvous', 'person'),)", 'object_name': 'Appointment'},
            'appointment_date': ('django.db.models.fields.DateTimeField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'deleted_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['card.Person']"}),
            'rendezvous': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['appointment.Place']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'appointment.place': {
            'Meta': {'object_name': 'Place'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'time_slot': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '15'})
        },
        'appointment.timetable': {
            'Meta': {'unique_together': "(('place', 'day'),)", 'object_name': 'Timetable'},
            'capacity': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'}),
            'day': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'timetables'", 'to': "orm['appointment.Place']"}),
            'since': ('django.db.models.fields.TimeField', [], {}),
            'until': ('django.db.models.fields.TimeField', [], {})
        },
        'card.person': {
            'Meta': {'object_name': 'Person'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'birth_day': ('django.db.models.fields.DateField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'document': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_death': ('django.db.models.fields.CharField', [], {'default': "'2'", 'max_length': '1'}),
            'kind_document': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'last_name_second': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'locality': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'neighborhood': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'record_day': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'record_month': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'record_year': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'sdm': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'vehicles': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'owner'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['card.Vehicle']"})
        },
        'card.vehicle': {
            'Meta': {'object_name': 'Vehicle'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'license_plate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'transit_entity': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['appointment']