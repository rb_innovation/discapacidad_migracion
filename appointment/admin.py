from django.contrib import admin

from appointment.models import Timetable, Place, Appointment

class TimetableAdmin(admin.ModelAdmin):
    pass

class TimetableInline(admin.TabularInline):
    model = Timetable
    extra = 0

class PlaceAdmin(admin.ModelAdmin):
    inlines = [TimetableInline]

class AppointmentAdmin(admin.ModelAdmin):
    list_display = ('person', 'rendezvous', 'appointment_date', )
    list_filter = ('rendezvous', 'appointment_date')
    search_fields = ('person__first_name', 'person__second_name', 'person__last_name', 'person__last_name_second', 'person__document')
    raw_id_fields = ('person',)
    ordering = ('rendezvous', 'appointment_date', )
    date_hierarchy = 'appointment_date'


admin.site.register(Timetable, TimetableAdmin)
admin.site.register(Place, PlaceAdmin)
admin.site.register(Appointment, AppointmentAdmin)
