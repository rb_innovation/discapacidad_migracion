# -*- coding: utf-8 -*-
from datetime import datetime,timedelta

from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MINUTELY, DAILY
from dateutil.parser import parse
from dateutil import tz

from django import forms
from django.db.models import Count
from django.utils import timezone
from django.utils.timezone import make_aware, now as tz_now
from django.template.defaultfilters import date as _date

from appointment.models import Place, Appointment

import psycopg2, psycopg2.extras

RRULE_OPTIONS = {
    'dtstart' : datetime.now()+relativedelta(days=1, hour=0, minute=0, second=0),
    'count': 7,
    'byhour': 0, 
    'byminute': 0, 
    'bysecond': 0,
    'byweekday': [0,1,2,3,4],
    'cache': False,
}

#PLACE_CHOICE = ()
#conn = psycopg2.connect(database='mapaz',user='mapas',password='123456789', host='conveniosqa.cr0k1tnuhdai.us-west-2.rds.amazonaws.com', port='5432')
#cur = conn.cursor()
#cur.execute("SELECT id,name  FROM customer_service_customization")
#row = cur.fetchall()
#tupla = tuple(row)

PLACE_CHOICE = ()
PLACE_CHOICES = ()


DAY_CHOICES = ()
TIME_CHOICES = () # Set dinamically

def get_time_choices(place_id, day):
    day = parse(day)
    place = Place.objects.get(id=place_id)
    timetable = place.timetables.all().get(day=day.weekday())
    since_time = timetable.since
    since_date = datetime(day.year, day.month, day.day, since_time.hour, since_time.minute, since_time.second)
    until_time = timetable.until
    until_date = datetime(day.year, day.month, day.day, until_time.hour, until_time.minute, until_time.second)
    TIME_RRULE_OPTIONS = {
        'interval': 15,
        'dtstart': since_date,
        'until': until_date,
    }
    
    appointment_qs = Appointment.objects.all().filter(appointment_date__day=day.day, rendezvous=place)
    appointment_qs = appointment_qs.values('appointment_date')
    appointment_qs = appointment_qs.annotate(appointments=Count('appointment_date', distinct=False))
    appointment_qs = appointment_qs.filter(appointments=timetable.capacity)
    appointment_qs = appointment_qs.values_list('appointment_date')

    exclude_dates = []
    for appointment_date_ in appointment_qs:
        ad = appointment_date_[0]
        from_zone = tz.gettz('UTC')
        to_zone = timezone.get_default_timezone()
        ad = ad.replace(tzinfo=from_zone)
        ad = ad.astimezone(to_zone)
        exclude_dates.append(ad)

    for time in rrule(MINUTELY, **TIME_RRULE_OPTIONS):
        t = make_aware(time, timezone.get_default_timezone())
        if not t in exclude_dates:
            yield (t.strftime("%Y-%m-%d %H:%M:%S"), t.strftime("%I:%M %p"))

def workdays(d, end, excluded=(6, 7)):
    days = []
    while d.date() <= end.date():
       if d.isoweekday() not in excluded:
           days.append(d)
       d += timedelta(days=1)
    return days

class AppointmentStep1(forms.Form):
    person_id = forms.IntegerField(widget=forms.HiddenInput)
    person_document = forms.CharField(max_length=200, widget=forms.HiddenInput)
    place_id = forms.ChoiceField(choices=PLACE_CHOICE, label="Punto")
    day = forms.ChoiceField(choices=DAY_CHOICES, label=u"Día")

    print('day ' + str(day) + ' place_id ' + str(place_id))
    def __init__(self, *args, **kwargs):
        print('AppointmentStep1 init')
        place_id= kwargs.pop('place_id', None)
        print(timezone.now())
        super(AppointmentStep1, self).__init__(*args, **kwargs)
        self.fields['place_id'].choices = [(place.id, unicode(place)) for place in Place.objects.filter(active=1)]
        #self.fields['day'].choices = [(date.strftime("%Y%m%d"), _date(date, "F j")) for date in rrule(DAILY, **RRULE_OPTIONS)]
        self.fields['day'].choices = [(date.strftime("%Y%m%d"), _date(date, "F j")) for date in workdays(timezone.now()+relativedelta(days=1, hour=0, minute=0, second=0),timezone.now()+relativedelta(days=10, hour=0, minute=0, second=0))]
        #print( self.fields['day'].choices)
        #print(type(date.strftime("%Y%m%d")))
        #print('end AppointmentStep1 init')
 
class AppointmentStep2(forms.Form):
    person_id = forms.IntegerField(widget=forms.HiddenInput)
    person_document = forms.CharField(max_length=200, widget=forms.HiddenInput)
    place_id = forms.IntegerField(widget=forms.HiddenInput)
    day = forms.CharField(max_length=8, widget=forms.HiddenInput)
    time = forms.ChoiceField(choices=TIME_CHOICES, label=u'Hora')

    def clean(self):
        cleaned_data = super(AppointmentStep2, self).clean()
        person_id = cleaned_data.get('person_id')
        place_id = cleaned_data.get('place_id')
        day = cleaned_data.get('day')
        
        person_qs = Appointment.objects.all().filter(person_id=person_id).filter(appointment_date__gt=tz_now())
        if person_qs.exists():
            raise forms.ValidationError("Esta persona ya tiene agendada una cita.")

        # day = parse(day)
        # place = Place.objects.get(id=place_id)
        # timetable = place.timetables.all().get(day=day.weekday())
        # appointment_count = Appointment.objects.all().filter(rendezvous=place_id).filter(appointment_date__day=day.day).count()
        # if appointment_count >= timetable.capacity:
        #     raise forms.ValidationError(u"Lamentablemente mientras el usuario agendaba la cita este punto agotó su capacidad de atención")

        return cleaned_data

    def __init__(self, *args, **kwargs):
        place_id = kwargs.pop('place_id', None)
        day = kwargs.pop('day', None)
        if place_id and day:
            self.base_fields['time'].choices = get_time_choices(place_id, day)
        super(AppointmentStep2, self).__init__(*args, **kwargs)