from datetime import datetime as d
import xlwt
from django.db import connection, transaction
import _mysql


def handle_uploaded_file(f):

    archivo="static/files/FiletoRead.csv"
    destination = open(archivo, 'w+')
    for chunk in f.chunks():
        destination.write(chunk)
	#print chunk.split(';')
    destination.close()

    contador= 0
    for linea in open(archivo).xreadlines( ): contador+= 1

    return contador


def handle_loaddata_file():

    # filterwarnings('ignore', category = Database.Warning)
    cursor = connection.cursor()

    print 'Proceso Mysql inicio {0}'.format(d.now())

    query_str="TRUNCATE TABLE __tmp_load_data;"
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    query_str="TRUNCATE TABLE card_novelty;"
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    query_str="TRUNCATE TABLE __tmp_data;"
    cursor.execute(query_str)
    transaction.commit_unless_managed()


    #Excecurte the LOAD sentence through mysql interface library to avoid the warnings
    connection_params = {                                                     
        'host': cursor.db.settings_dict['HOST'],
        'user': cursor.db.settings_dict['USER'],
        'passwd': cursor.db.settings_dict['PASSWORD'],
        'db': cursor.db.settings_dict['NAME'],
        'local_infile': 1,
    }

    db=_mysql.connect(**connection_params)

    #Carga toda la data enviada en el archivo de novedades mensuales
    query_str = """
        LOAD DATA LOCAL INFILE 'static/files/FiletoRead.csv' 
        INTO TABLE handicapcard.__tmp_load_data 
        CHARACTER SET latin1 
        FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '\"' 
        IGNORE 1 LINES 
        (  
            record_day,  
            record_month,  
            record_year,  
            first_name,  
            second_name,  
            last_name,  
            last_name_second,  
            kind_document,  
            document,  
            @birth_day,  
            address,  
            locality, 
            neighborhood,  
            is_death,  
            sdm,  
            license_plate,  
            transit_entity,  
            change_desc,  
            observation  
        ) 
        SET birth_day = STR_TO_DATE(@birth_day, '%%d/%%m/%%Y' );
    """
    db.query(query_str)
    db.close()

    #Selecciona los registros que ya existen en la base de datos (card_person) y les asigna el respectivo id
    query_str="""
        UPDATE __tmp_load_data a, card_person b 
        SET a.id=b.id 
        WHERE a.document=b.document;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    #Actualiza novedad para los registros nuevos --> Actualiza el documento
    query_str="""
        UPDATE __tmp_load_data a  
        SET 
            a.document=TRIM(BOTH '\r' FROM a.observation), 
            a.kind_document=a.change_desc 
        WHERE a.change_desc !='' and a.observation !='' 
            AND a.change_desc IN ('CC','TI','RC','PA') 
            AND a.id=0;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    #Selecciona los registros que ya existen en la base de datos (card_person), evita repetidos
    query_str="""
        UPDATE __tmp_load_data a, card_person b 
        SET a.id=b.id 
        WHERE a.document=b.document 
            AND a.change_desc !='' 
            AND a.observation !='' 
            AND a.change_desc IN ('CC','TI','RC','PA') 
            AND a.id=0;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    #Inserta en card_person los nuevos registros, toma los registros sin id de __tmp_load_data
    query_str="""
        INSERT INTO card_person 
        SELECT 
            NULL,
            record_day,
            record_month,
            record_year,
            first_name,
            second_name,
            last_name,
            last_name_second,
            kind_document,
            document,
            birth_day,
            address,
            locality,
            neighborhood,
            is_death,
            NOW(),
            NOW() 
        FROM __tmp_load_data 
        WHERE id not IN ( SELECT id FROM card_person ) 
        GROUP BY document;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    #card_novelty
    query_str="""
        INSERT INTO card_novelty 
        SELECT 
            NULL, 
            sdm, 
            id 
        FROM __tmp_load_data 
        WHERE id!=0 
            AND sdm IN (1,2,3) 
        GROUP BY id, sdm;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    #card_vehicle
    query_str="""
        INSERT IGNORE INTO card_vehicle 
        SELECT 
            NULL,
            license_plate, 
            transit_entity 
        FROM __tmp_load_data 
        WHERE sdm != '';
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    #card_person_vehicles
    query_str="""
        INSERT IGNORE INTO card_person_vehicles 
        SELECT 
            NULL, 
            a.id person_id, 
            b.id vehicle_id 
        FROM __tmp_load_data a, card_vehicle b 
        WHERE a.license_plate=b.license_plate 
            AND a.transit_entity=b.transit_entity 
            AND a.id!=0 
            AND a.sdm IN (1,2,3) 
        GROUP BY b.id;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    #Actualiza cambios de datos en los registros antiguos 
    query_str="""
        UPDATE card_person a, __tmp_load_data b 
        SET 
            a.record_day=b.record_day, 
            a.record_month=b.record_month, 
            a.record_year=b.record_year, 
            a.first_name=b.first_name, 
            a.second_name=b.second_name, 
            a.last_name=b.last_name, 
            a.last_name_second=b.last_name_second, 
            a.kind_document=b.kind_document, 
            a.document=b.document, 
            a.birth_day=b.birth_day, 
            a.address=b.address, 
            a.locality=b.locality, 
            a.neighborhood=b.neighborhood, 
            a.is_death=b.is_death 
        WHERE a.id=b.id;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    #Actualiza la novedad de cambio de documento (tipo-numero) en registros antiguos
    query_str = """
        UPDATE __tmp_load_data a, card_person b 
        SET 
            b.document=trim(BOTH '\r' from a.observation), 
            b.kind_document=a.change_desc 
        WHERE a.change_desc !='' 
            AND a.observation !='' 
            AND a.change_desc in ('CC','TI','RC','PA') 
            AND a.id=b.id;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    query_str = """
        INSERT INTO __tmp_data 
        SELECT 
            kind_document, 
            document, 
            NULL, 
            NULL 
        FROM card_person 
        ORDER BY document;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    query_str = """
        DELETE FROM __tmp_data 
        WHERE kind_document IN ('TI', 'CC') 
            AND document NOT RLIKE '^[0-9]*$';
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    query_str = """
        UPDATE __tmp_data a, card_person b, card_novelty c 
        SET 
            a.amount='0', 
            a.state='excluido' 
        WHERE a.document=b.document 
            AND b.id=c.person_id;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    query_str = """
        UPDATE __tmp_data a, card_person b 
    	SET
    	    a.amount='0',
    	    a.state='excluido'
        WHERE a.document=b.document 
            AND b.is_death='2';
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    query_str = """
        UPDATE __tmp_data 
        SET amount='17000', state='activo' 
        WHERE amount IS NULL 
            AND state IS NULL;
    """
    cursor.execute(query_str)
    transaction.commit_unless_managed()

    print 'Mysql Final {0}'.format(d.now())


def handle_write_excelfile():

    cursor = connection.cursor()
    
    count=0
    hoja=0
    title = ['Tipo de Documento','Documento','Monto','Estado']
    forma = ['@','0','0','@']

    print 'Proceso Excel inicio {0}'.format(d.now())

    wb = xlwt.Workbook()
    style = xlwt.XFStyle()

    #Excecurte the SELECT sentence through mysql interface library to avoid the warnings
    connection_params = {                                                     
        'host': cursor.db.settings_dict['HOST'],
        'user': cursor.db.settings_dict['USER'],
        'passwd': cursor.db.settings_dict['PASSWORD'],
        'db': cursor.db.settings_dict['NAME'],
        'local_infile': 1,
    }

    db=_mysql.connect(**connection_params)

    query_str = """
        SELECT * 
        FROM __tmp_data 
        WHERE document!='' 
        ORDER BY CONVERT(document,UNSIGNED INTEGER) DESC;
    """
    db.query(query_str)
    result=db.store_result()

    for row in result.fetch_row(maxrows=0):
        # print row

        if count==0:
            sheet = wb.add_sheet("hoja {0}".format(hoja))

            for j in xrange(4):
                sheet.write(0,j,title[j])

        count+=1

        for j in xrange(4):
            style.num_format_str = forma[j]
            sheet.write(count,j,row[j],style)

        if count==64999:
            count=0
            hoja+=1

    wb.save('static/files/GenFile.xls')
    response = result.num_rows()
    db.close()
    print 'Proceso Excel final {0}'.format(d.now())
    return response
