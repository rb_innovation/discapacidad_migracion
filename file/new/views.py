from django.shortcuts import redirect, render
from forms import UploadFileForm
from file.utils import handle_uploaded_file, handle_loaddata_file, handle_write_excelfile

from django.contrib import auth
from django.contrib.auth.decorators import login_required


def login(request):
    context = {}
    MSG = False
    print request.POST
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            return redirect('archivo')
        else:
            MSG = True
    context['MSG']=MSG
    return render(request,'file/login.html', context)


def logout(request):
    auth.logout(request)
    return redirect("login")


@login_required
def archivo(request):
    datos = {}
    if request.method=='POST':
        formUpFile = UploadFileForm(request.POST, request.FILES)
        if formUpFile.is_valid():
            RegFile=handle_uploaded_file(request.FILES['file'])
            data=handle_loaddata_file()
            Genefile=handle_write_excelfile()
            datos['nombFile'] = Genefile
            datos['RegFile'] = RegFile
    else:
        formUpFile = UploadFileForm()
        datos['filePath'] = "No seleccionado"

    datos['formUpFile'] = formUpFile
    return render(request, 'file/FileformUpload.html', datos)


