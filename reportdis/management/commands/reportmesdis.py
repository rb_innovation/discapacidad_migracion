# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.mail import EmailMessage
from django.db import connections, transaction

import csv
import datetime
import os
import sys
import time

QUERY_ROOT = getattr(settings, 'QUERY_ROOT')
EMAIL_HOST_USER = getattr(settings, 'EMAIL_HOST_USER')
REPORT_EMAIL_RECIPIENTS = getattr(settings, 'REPORT_EMAIL_RECIPIENTS')
BODY_EMAIL = getattr(settings, 'BODY_EMAIL')


class Command(BaseCommand):
    def handle(self, *args, **options):
        #Capturo la fecha
        fecha = datetime.date.today() + datetime.timedelta(days=-1)
        fecha = fecha.strftime("%Y-%m")
        anio = fecha[:-3]
        mes = fecha[-2:]
        print anio
        print mes

        csvsalida = open(r'/tmp/ReporteCitasDiscapacitados'+anio+mes+'.csv', 'w')
        exit = csv.writer(csvsalida, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        #Titulos del csv
        titles = [u'Cantidad', u'day', u'name']
        exit.writerow(titles)

        #Consultando BD
        with open(QUERY_ROOT + "/query.sql", 'r') as _file:
            query = _file.read().format(anio = anio, mes = mes)
        print "Consultando sisudb..."
        print query

        cursor = connections['default'].cursor()

        cursor.execute(query)
        row = cursor.fetchall()

        print "Construyendo ReporteCitasDiscapacitados"+anio+mes+".csv..."

        for line in row:
            arr = []
            for index in range(len(line)):
                arr.append(line[index])
            for n, item in enumerate(arr):
                if n ==2:
                    if item is not None:
                        if "," in item:
                            item = item.replace(",", ".")
                        arr[n] = item.encode('utf-8')

            exit.writerow(arr)

        csvsalida.close()

        print 'Envío de correos...'

        attach_path = [r'/tmp/ReporteCitasDiscapacitados'+anio+mes+'.csv',]

        email_options = {
            'subject': 'Reporte Citas Discapacitados '+anio+'-'+mes+'',
            'body': BODY_EMAIL[0].encode('utf-8'),
            'from_email': EMAIL_HOST_USER,
            'to': REPORT_EMAIL_RECIPIENTS,
        }
        email = EmailMessage(**email_options)
        for index in range(len(attach_path)):
            email.attach_file(attach_path[index])
        email.send(fail_silently=False)
        print "Command successfully executed"
