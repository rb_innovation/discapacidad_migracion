from django.conf.urls import patterns, include, url
#from django.views.generic.simple import direct_to_template
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'card.views.root_index', name='root-index'),
    url(r'^tarjeta/', include('card.urls')),
    url(r'^faq/$', TemplateView.as_view(template_name= 'faq.html'), name='faq'),
    url(r'^secretaria-movilidad/$', TemplateView.as_view(template_name= 'movilidad.html'), name='movilidad'),
    url(r'^secretaria-salud/$', TemplateView.as_view(template_name= 'salud.html'), name='salud'),
    url(r'^secretaria-integracion-social/$', TemplateView.as_view(template_name= 'integracion.html'), name='integracion'),
    url(r'^cita/', include('appointment.urls')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^archivo/$', 'file.views.login', name='login'),
    #url(r'^archivo/logout$', 'file.views.logout', name='logout'),
    #url(r'^archivo/carga$', 'file.views.archivo', name='archivo'),
    url(r'^archivo/$', 'file.views.archivo', name='archivo'),
)
