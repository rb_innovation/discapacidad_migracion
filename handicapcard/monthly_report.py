# -*- coding: utf-8 -*-
import time, datetime
import MySQLdb
import local_settings
import codecs
import csv
import unicodedata
#import re

import smtplib, os
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from smtplib import SMTP


HOST = local_settings.REP_HOST
USER = local_settings.REP_USER
PASSWORD = local_settings.REP_PASSWORD
DATABASE = local_settings.REP_DATABASE
ROUTE = local_settings.REPORT_ROUTE

GENERAL_DATE = datetime.date.today() + datetime.timedelta(days=-30)
YESTERDAY_DATE = GENERAL_DATE.strftime("%m/%Y")
YEAR = YESTERDAY_DATE[-4:]
MONTH = YESTERDAY_DATE[:-5]

'''
def stringNormalice(message):
    return re.sub(
        r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1", 
        normalize( "NFD", str(message)), 0, re.I
    )
'''
def csv_generate():
    print (GENERAL_DATE)
    print (YEAR)
    print (MONTH)

    file_path = ROUTE + r'/ReporteCitasDiscapacitados'+ YEAR + MONTH +'.csv'

    title = ['Cantidad','day','name']

    db = MySQLdb.connect(host=HOST,user=USER,passwd=PASSWORD,db = DATABASE)

    cursor = db.cursor()

    query_str = """ 	SELECT 
        count(appointment_appointment.id) AS Cantidad, 
        DAY(appointment_appointment.appointment_date) AS day,
        appointment_place.name as Estacion
    FROM appointment_appointment, appointment_place
    WHERE
        YEAR(appointment_appointment.appointment_date) = YEAR(NOW()) 
        AND 
        MONTH(appointment_appointment.appointment_date) = MONTH(NOW()) -1
        AND 
        appointment_place.id=appointment_appointment.rendezvous_id
    GROUP BY DAY(appointment_appointment.appointment_date), appointment_appointment.rendezvous_id;"""
    
    print 'Proceso CSV inicio '+str(datetime.datetime.now())
    print query_str
    cursor.execute(query_str)
    resultado = cursor.fetchall()
    tupla = tuple(resultado)
    print tupla
    with codecs.open(file_path, "wb", "utf-8-sig") as csv_file:

        cells = []

        for header in title:
            cells.append(u'{0}'.format(header))

        cells.append(u'\n')
        cell = cells[:-1]
        csv_file.write(u','.join(cell))
        csv_file.write(u'\n')
        
        for refund in tupla:
 
            cells = []
            cells.append(u'{0}'.format(refund[0]))
            cells.append(u'{0}'.format(refund[1]))
            cells.append(u'{0}'.format(refund[2]))
            cells.append(u'\n')
            csv_file.write(u','.join(cells))

    print 'Proceso CSV final '+str(datetime.datetime.now())
    send_mail()

def send_mail():
    msg = MIMEMultipart()

    sender = local_settings.EMAIL_HOST_USER
    recipients = local_settings.REPORT_EMAIL_RECIPIENTS
    msg['Subject'] = 'Reporte de Discapacitados '+ YEAR + '-'+ MONTH
    msg['From'] = local_settings.EMAIL_HOST_USER
    msg['To'] = ", ".join(recipients)
    

    # Esto es lo que se ve si uno no tiene un lector de mails como la gente:
    msg.preamble = 'Mensaje de multiples partes.\n'

    # Esta es la parte textual:
    part = MIMEText("Mensaje creado automaticamente con el listado de citas de discapacidad reportadas durante "+ YEAR + "/"+ MONTH)
    msg.attach(part)
    file_path = ROUTE + r'/ReporteCitasDiscapacitados'+ YEAR + MONTH +'.csv'

    # Esta es la parte binaria (puede ser cualquier extension):
    part = MIMEApplication(open(file_path).read())
    part.add_header('Content-Disposition', 'attachment', filename = 'ReporteCitasDiscapacitados'+ YEAR + MONTH +'.csv')
    msg.attach(part)

    # Se pueden seguir agregando partes (texto, imagenes, datos binarios, etc.)

        # Crear una instancia del servidor para envio de correo (hacerlo una sola vez)
    #smtp = smtplib.SMTP('smtp.gmail.com:587')
    #smtp = smtplib.SMTP('172.30.1.237:25')
    smtp = smtplib.SMTP('10.11.2.126:25')#qa
    # Iniciar sesion en el servidor (si es necesario):
    #smtp.starttls()
    #smtp.ehlo()
    #smtp.login(local_settings.EMAIL_HOST_USER, local_settings.EMAIL_HOST_PASSWORD)

    # Enviar el mail (o los mails)
    smtp.sendmail(sender, recipients, msg.as_string())
    #smtp.sendmail(msg['From'], msg['To'], msg.as_string())

    print "Enviado"

csv_generate()