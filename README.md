# PROYECTO DISCAPACIDAD (Guía de instalación y actualización para ambiente de pruebas y producción)

En este README se presentan las instrucciones para instalar y ejecutar la aplicación de discapacidad en un servidor de producción o de pruebas. 

La logica de esta instalación para no tener conflictos de librería pyhton entre versiones 2.7 y 3 ni en la versión de las librerías utilizadas pora cada aplicación es la siguiente:

* Se utiliza el python3 instalado en el sistema operativo por defecto solo se le agregan los paquetes virtualenv y virtualenvwrapper

* Dado que la aplicación está desarrollada para python 2.7 se crea un ambiente virtual que utiliza python 2.7 y al que se le instala unicamente uWSGI. El uWSGI se correrá desde este ambiente virtual y se configura en modo "emperor" para poder correr varias aplicaciones a la vez.  

* Se crea un ambiente virtual por cada aplicación con python 2.7 o python3 según sea la necesidad de cada una. De este modo cada uno tiene su propia versión de interprete de python y las librerías de las que depende.


La siguiente es la estructura de directorios y archivos que se manejarán:

```
├── /home                        
│   ├── desarrollorb                            <- Directorio raiz del usuario ... equivale a ~/
│   │   ├── .vimrc                              <- Archivo de configuración del editor vi
│   │   ├── .profile                            <- Archivo para establecer configuraciones particulares para el usuario desarrollorb
│   │   ├── .bashrc                             <- Archivo ejecutar scripts particulares para el usuario desarrollorb
│   │   ├── .ssh                                <- Directorio de configuración del ssh
│   │   │   ├── bitbucket                       <- Llave privada para acceso a bitbucket
│   │   │   ├── bitbucket.pub                   <- Llave pública para acceso a bitbucket (se pone en bitbucket)
│   │   │   ├── config                          <- Archivo de configuración del ssh
│   │   ├── .virtualenvs                        <- Carpeta que contiene los virtual environments de python
│   │   │   ├── venv-uwsgi-py-2.7               <- Carpeta del ambiente virtual para uwsgi
│   │   │   ├── venv-discapacidad           <- Carpeta del ambiente virtual para la aplicacion 
│   │   │   ├── venv-...          				<- Carpeta del ambiente virtual de otras aplicaciones
│   │   ├── www                                 <- Carpeta que contiene las aplicaciones 
│   │   │   ├── discapacidad_migracion                <- Carpeta de codigo del proyecto
│   │   │   │   ├── handicapcard                        <- Carpeta principal del proyecto 
│   │   │   │   ├── manage.py               <- Programa principal del proyecto
│   │   │   │   ├── appointment        <- Carpeta con los archivos del proyecto
│   │   │   │   ├── card        <- Carpeta con los archivos del proyecto
│   │   │   │   ├── file        <- Carpeta con los archivos del proyecto
│   │   │   │   ├── reportdis        <- Carpeta con los archivos del proyecto
│   │   │   │   ├── README.md                   <- Archivo de descripción del proyecto
│   │   │   │   ├── requirements.txt            <- Archivo que lista las librerías requeridas por el proyecto
│   │
├── /etc                        
│   ├── hosts									<- Archivo de configuración del DNS local
│   ├── nginx
│   │   │   ├── nginx.conf                      <- Archivo que contiene la configuración general de nginx (no se modifica)
│   │   │   ├── sites-enabled                   <- Carpeta que contiene los enlaces simbolicos de los archivos de configuración de las aplicaciones que está sirviendo nginx
│   │   │   │   ├── discapacidad.conf       <- Enlace simbolico a la aplicación.  
│   │   │   │   ├── [app_name]_backend.conf     <- Enlace simbolico a otras aplicaciones de backend  
│   │   │   ├── sites-available                 <- Carpeta que contiene los archivos de configuración de todas las aplicaciones (activas e inactivas)
│   │   │   │   ├── discapacidad.conf       <- Archivo de configuración de nginx para el proyecto
│   │   │   │   ├── [app_name]_frontend.conf    <- Archivo de configuración de nginx para el frontend de otras aplicaciones
│   ├── uwsgi
│   │   │   ├── apps-enabled                    <- Carpeta que contiene los enlaces simbolicos de los archivos de configuración de las aplicaciones que está sirviendo uwsgi
│   │   │   │   ├── discapacidad.ini        <- Enlace simbolico a la aplicación.  
│   │   │   │   ├── [app_name]_backend.ini      <- Enlace simbolico a la aplicación de backend de otras aplicaciones  
│   │   │   ├── apps-available                  <- Carpeta que contiene los archivos de configuración de todas las aplicaciones (activas e inactivas)
│   │   │   │   ├── discapacidad.ini        <- Archivo de configuración de uwsgi para el  proyecto 
│   │   │   │   ├── [app_name]_backend.ini      <- Archivo de configuración de uwsgi para el backend de otras aplicaciones
│   ├── systemd
│   │   │   ├── system
│   │   │   │   ├── uwsgi.service				<- Archivo de arranque, reinicio y detención del servicio de wsgi
│   │
├── /var                        
│   ├── log									
│   │   ├── uwsgi 								<- Carpeta de logs de uwsgi
│   │   │   ├── emperor.log 					<- Archivo de log del proceso uwsgi emperor que controla el ciclo de vida de las aplicaciones
│   │   │   ├── discapacidad_uwsgi.log 		<- Archivo de log del proceso uwsgi que controla la aplicación del backend del proyecto
│   │   │   ├── [app_name]_backend.log 			<- Archivo de log del proceso uwsgi que controla la aplicación del backend de otras aplicaciones
│   │   ├── nginx 								<- Carpeta de logs de nginx
│   │   │   ├── discapacidad_access.log	<- Archivo de log de nginx que registra las peticiones del cliente para la aplicación
│   │   │   ├── [app_name]_backend_access.log	<- Archivo de log de nginx que registra las peticiones del cliente para otras aplicaciones
│   │   │   ├── discapacidad_error.log		<- Archivo de log del proceso uwsgi que controla la aplicación
│   │   │   ├── [app_name]_backend_error.log	<- Archivo de log del proceso uwsgi que controla la aplicación del backend para otras aplicaciones
│   │   ├── rb_apps 							<- Carpeta de logs de las aplicaciones
│   │   │   ├── discapacidad                <- Carpeta de los logs de la aplicación
│   │   │   │   ├─ [date]discapacidad.log <- Log de la aplicación
│   │   │   ├── ...	                            <- Carpeta de los logs de otras aplicaciones
│   │   ├── syslog 								<- Log general del sistema operativo, en el se encuentran los logs de los crones
│   ├── spool
│   │
├── /tmp 										<- Carpeta de archivos temporales del sistema, se borra el contenido al reiniciar                       
│   ├── discapacidad.sock 					<- Archivo que representa el socket por el que se comunica el nginx con el uwsgi
│   ├── discapacidad.pid 					<- Archivo que contiene el id de proceso python de la aplicación
│   ├── [app_name]_backend.sock 				<- Archivo que representa el socket por el que se comunica el nginx con el uwsgi de otas aplicaciones
│   ├── [app_name]_backend.pid 					<- Archivo que contiene el id de proceso python de la aplicación del backend de otas aplicaciones
```

## Tabla de contenido
[TOC]

## Instalación por primera vez

### Prerequisitos

* Se asume que se cuenta con un servidor provisto por infraestructura con el sistema operativo ubuntu 16.04 LTS con el usuario desarrollorb y su respectiva llave .pem para el acceso al servidor.

* Asi mismo que se cuenta con la instalación local de un linux ubuntu 16.04 LTS desde donde se va a hacer la conexión al servidor provisto por infraestructura.

* Se asume que las url de los subdominios de pruebas (discapacidad.qa.aws.rbsas.co) y producción (discapacidad.aws.rbsas.co) están registradas en el DNS y redirigen a las maquinas respectivas de pruebas y producción. En el caso en el que los dominios no estén registrados todavía en el DNS, deberán registrarse en el archivo /etc/hosts temporalmente.


### Configuración de las llaves de acceso al servidor en la maquina local
Para configurar el acceso por ssh se debe hacer los siguiente:

* Renombrar el archivo de la llave .pem entregado por infraestructura asi:
    * Para producción: discapacidad_prod.pem
    * Para pruebas: discapacidad_prue.pem

* Copiar la llave en la maquina local en la carpeta ~/.ssh
* Conectarse al servidor de producción asi:
    * ```ssh -i "~/.ssh/discapacidad_prod.pem" desarrollorb@[nombre_servidor_producción]```

* Conectarse al servidor de pruebas asi:
    * ``` ssh -i "~/.ssh/discapacidad_prue.pem" desarrollorb@[nombre_servidor_pruebas] ```
    
### Configuración para uso del editor vi
* Una vez ingresado al servidor crear el archivo .vimrc y agregarle la siguiente línea asi:
    * vi ~/.vimrc
    * Linea: set nocompatible
    

### Configuración de la zona horaria
* Ejecutar el siguiente comando para agregar la linea al archivo .profile Los cambios surtiran efecto al hacer login nuevamente al servidor.
    * ```echo "TZ='America/Bogota'; export TZ" >> ~/.profile ```
    * ```echo "LC_ALL=es_CO.UTF-8; export LC_ALL" >> ~/.profile ```

* Ejecutar el siguiente comando.
    * ``` sudo dpkg-reconfigure locales ```
    
Seleccionar es_CO.UTF-8

### Instalar paquete NTP para mantener el reloj del sistema sincronizado por Network Time Protocol
* ``` sudo apt install ntp ```

### Instalación y configuración de git y descarga del código
Una vez conectado al servidor de producción o pruebas según sea el caso:

#### Instalar git
* ``` sudo apt-get install git ```

#### Configurar git para acceso a bitbucket
* Crear un par de llaves publicas y privadas rsa para poder clonar los repositorios
    * En linea de comandos digitar: ```ssh-keygen```
        * El sistema sugiere la ruta (/home/desarrollorb/.ssh/id_rsa), ignorarlo y digitar 
            ```/home/desarrollorb/.ssh/bitbucket```
        * No digitar password y presionar enter
            * Se generarán las llaves privada y publica bitbucket y bitbucket.pub en ~/.ssh/
            
* Indicarle al sistema qué llave utilizar al conectarse con bitbucket.org
    * Crear si no existe el archivo ```config``` en ~/.ssh/ asi:
        * ```vi ~/.ssh/config``` y agregar el siguiente contenido
            * ```Host bitbucket.org```
            * ```StrictHostKeyChecking no```
            * ```IdentityFile ~/.ssh/bitbucket```
            

* Agregar la llave pública a cada uno de los repositorios en bitbucket (Se debe tener permisos de administrador)
    * Descargar la llave publica ubicada en ```more ~/.ssh/bitbucket.pub``` del servidor de pruebas asi:
        * scp -r -i ~/.ssh/discapacidad_prue.pem desarrollorb@nombre_servidor:/home/desarrollorb/.ssh/bitbucket.pub .
    * Descargar la llave publica ubicada en ```more ~/.ssh/bitbucket.pub``` del servidor de producción asi:
        * scp -r -i ~/.ssh/discapacidad_prod.pem desarrollorb@nombre_servidor:/home/desarrollorb/.ssh/bitbucket.pub .
    * Copiar el contenido de la llave publica en el portapapeles 
    * Ir al respositorio rb_innovation/Discapacidad_Migracion
        * En la opción Settings -> Access Key poner como label: ubuntu-16.04-pruebas-discapacidad o ubuntu-16.04-produccion-discapacidad según el servidor que se esté configurando 
        * Copiar el contenido de la llave en Bitbucket en la casilla Key* y presionar el boton Add Key


#### Obtener el codigo a desplegar

* En bitbucket ir al repositorio a rb_innovation/Discapacidad_Migracion y seleccionar el botón “Clone”, copiar el comando de clonación git.

* Crear y cambiarse a la carpeta de nginx donde se va poner el código del repositorio 
    * ```mkdir ~/www```
    * ``` cd ~/www ```
    
* Ejecutar el comando de clonación del repositorio. Ej:
    * ``` git clone git@bitbucket.org:rb_innovation/discapacidad_migracion.git```
    
* Cambiar al branch de desarrollo: (Esto solo para el caso en que el que se esté desplegando el ambiente de pruebas)
    * ```cd discapacidad_migracion```
    * ``` git checkout develop  ```
     
### Instalación de nginx
A continuación se instalará la ultima versión estable de nginx. 

* Se ejecutan los siguientes comandos:
    * ``` sudo apt-get install software-properties-common #instala el programa add-apt-repository```
    * ``` sudo add-apt-repository ppa:nginx/stable```

* Presionar enter cuando se solicite confirmación
    * ``` sudo apt-get update```
    * ``` sudo apt-get upgrade```
    * ``` sudo apt-get install nginx```


La versión instalada puede verificarse con el comando:
    * ``` nginx -V ```

#### Configuración de Nginx para leer los archivos de configuración de cada aplicación
* Verificar que el archivo de configuración principal de nginx /etc/nginx/nginx.conf contenga la linea```include /etc/nginx/sites-enabled/*;``` la cual le indica la ruta de donde debe tomar los archivos de configuración de los sitios web que debe servir.
    * ``` more /etc/nginx/nginx.conf  |grep sites ```

* Verificar la existencia de las carpetas sites-enabled y sites-available
    * ``` ls -la /etc/nginx/ ```
    
En la carpeta sites-enabled se pondrán enlaces simbolicos a los archivos de configuración de las aplicación que debe servir nginx, 
ubicados en sites-available. En sites-available podrán encontrarse mas archivos de configuración, pero solo los enlazados mediante
enlaces simbolicos desde sites-available son los que se van a servir.

### Configuración de Nginx para servir el frontend

#### Otorgar permisos a nginx a la carpeta del codigo
* ``` sudo chown www-data:www-data ~/www ```
* ``` sudo chown -R www-data:www-data ~/www/discapacidad_migracion```
* ``` sudo chmod 755 ~/www ```
* ``` sudo chmod 755 ~/www/discapacidad_migracion```


### Instalar paquetes python requeridos

* ``` sudo apt-get update && sudo apt-get -y upgrade```
* ``` sudo apt-get install git postgresql postgresql-contrib python-dev python3-pip```
* ``` sudo apt-get install python-dev libmysqlclient-dev```
* ``` pip3 install virtualenv```
* ``` pip3 install virtualenvwrapper ```

### Ambientes virtuales 
* Crear la carpeta que va a contener la configuración y librerías de los ambientes virtuales
    * ``` mkdir ~/.virtualenvs ```

* Editar .bashrc
    * ``` vi ~/.bashrc ```
    
* Agregar al final del archivo las lineas siguientes así:
    * ``` export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 ```
    * ``` export WORKON_HOME=~/.virtualenvs```
    * ``` source ~/.local/bin/virtualenvwrapper.sh``` 

* Activar cambios del .bashrc
    * ``` source ~/.bashrc ```
    
* Crear un virtualenv para uwsgi con python 2.7

    * ``` mkdir ~/.virtualenvs/venv-uwsgi-py-2.7 ```
    * ``` mkvirtualenv -a ~/.virtualenvs/venv-uwsgi-py-2.7 --python=/usr/bin/python venv-uwsgi-py-2.7 ```
    * ``` deactivate ```

* Crear un virtualenv para la aplicación de backend con python 2.7
    * ``` mkvirtualenv -a ~/www/discapacidad_migracion --python=/usr/bin/python venv-discapacidad```
    * ``` deactivate```

* Probar activar el virtualenv
    * ``` workon venv-uwsgi-py-2.7 ```
    
* Desactivar el virtualenv
    * ``` deactivate ```

### Instalación de requisitios aplicación de frecuencias

* ``` workon venv-discapacidad ```
* ``` pip install -r requirements.txt ```
* ``` python manage.py collectstatic ```
* ```deactivate```

### Instalación de uWSGI
A continuación se instalará el uwsgi y Django en el ambiente virtual 

* ```workon venv-uwsgi-py-2.7```
* ``` pip install uwsgi```
* ``` deactivate```

#### Crear carpetas de uWSGI
* ``` sudo mkdir -p /etc/uwsgi/apps-enabled /etc/uwsgi/apps-available /var/log/uwsgi/ ```

#### Crear archivo de arranque de uWSGI
* ``` sudo vi /etc/systemd/system/uwsgi.service ```

#### Contenido de archivo de arranque
    * ``` [Unit]```
    * ```Description=uWSGI application server in Emperor mode```
    * ```[Service]```
    * ```ExecStartPre=/bin/bash -c 'mkdir -p -m1774 /var/log/uwsgi; chown www-data:www-data /var/log/uwsgi'```
    * ```ExecStart=/home/desarrollorb/.virtualenvs/venv-uwsgi-py-2.7/bin/uwsgi --emperor /etc/uwsgi/apps-enabled --die-on-term --master --logto /var/log/uwsgi/emperor.log --uid www-data --gid www-data --enable-threads```
    * ```ExecStop=systemctl stop uwsgi.service```
    * ```Restart=always```
    * ```KillSignal=SIGQUIT```
    * ```Type=notify```
    * ```NotifyAccess=all```
    
    * ```[Install]```
    * ```WantedBy=multi-user.target```
                             
    

#### Aplicar la configuración del archivo uwsgi.service
* ``` sudo systemctl daemon-reload ```

### Configuración de uWSGI para servir el backend

#### Crear carpeta de logs de la aplicación con los permisos requeridos
* ``` sudo mkdir -p -m774 /var/log/rb_apps/discapacidad ```

#### Otorgar permisos a uWSGI a las carpetas necesarias
* ``` sudo chown www-data:www-data /var/log/rb_apps/discapacidad```
* ``` sudo chown www-data:www-data /var/log/uwsgi```


#### Agregar el usuario desarrollorb al grupo www-data para que pueda escribir y leer el log
* ```sudo usermod -a -G www-data desarrollorb```

Nota: despues de agregar el usuario desarrollorb al grupo www-data se debe hacer logout 
para que los cambios surtan efecto

#### Forzar a que los archivos nuevos hereden el grupo de la carpeta
* ``` sudo chmod g+s /var/log/rb_apps/discapacidad #Los archivos creados en el directorio heredan el grupo de la carpeta ```

#### Archivo de configuración del backend para uWSGI

* En la ruta ```/etc/uwsgi/apps-available``` crear el archivo mapa_red_recarga.ini asi:
    * ```sudo vi /etc/uwsgi/apps-available/discapacidad.ini```

* El archivo contendrá los siguiente
    * ```[uwsgi]```
    * ```protocol = uwsgi```
    * ```socket = /tmp/discapacidad.sock```
    * ```chmod-socket = 660```
    * ```pidfile = /tmp/discapacidad.pid```
    * ```spooler = /tmp```

    * ```# Application virtual environment folder full path```
    * ```venv = /home/desarrollorb/.virtualenvs/venv-discapacidad/```
    * ```# Django application base directory full path```
    * ```chdir = /home/desarrollorb/www/discapacidad_migracion/```
    * ```# Django wsgi file (application entry point)```
    * ```wsgi-file = handicapcard/wsgi.py```
    
    * ```master = true```
    * ```workers = 1```
    * ```limit-as = 3000```
    * ```vacuum = true```
    * ```stopsignal = SIGQUIT```
    * ```threads = 2```
    * ```buffer-size = 32768```
    
    * ```stats-http = true```
    * ```stats = :9011  # Ojo!! este puerto debe ser diferente para cada aplicación```
    * ```memory-report = true```

    * ```# log```
    * ```daemonize = /var/log/uwsgi/discapacidad.log```
    * ```logfile-chmod = 774```
    * ```log-reopen = true```

* Ajustar permisos y crear el enlace simbolico de apps-enabled hacia apps-available del archivo discapacidad.ini
    * ``` sudo ln -s /etc/uwsgi/apps-available/discapacidad.ini /etc/uwsgi/apps-enabled/discapacidad.ini ```
    * ``` sudo chgrp www-data /etc/uwsgi/apps-available/discapacidad.ini```
    * ``` sudo chmod 774 /etc/uwsgi/apps-available/discapacidad.ini```


#### Arrancar y detener el servicio de uWSGI
* ``` sudo service uwsgi start ```
* ``` sudo service uwsgi stop```


#### Comprobar el arranque del servidor uwsgi verificando el log del emperor
* ``` sudo tail -n 100 /var/log/uwsgi/emperor.log```

#### Comprobar que al cargar la aplicación no se hayan generado errores
* ``` sudo tail -n 100 /var/log/uwsgi/discapacidad.log ```

### Configuración de Nginx para servir el backend

#### Archivo de configuración del backend para nginx
* Crear el archivo de configuración asi:
```sudo vi /etc/nginx/sites-available/discapacidad.conf```

* El archivo contendrá los siguiente

```
server {
    listen      80;
    server_name  discapacidad.rbsas.co;        #url producción
    #server_name  discapacidadqa.rbsas.co;        #url pruebas
    return 301 https://$host$request_uri;
}
server {
    listen      443 ssl;
    server_name  discapacidad.rbsas.co;        #url producción
    #server_name    discapacidadqa.rbsas.co;   #url pruebas
    charset     utf-8;

    location / {
        uwsgi_pass  unix:///tmp/discapacidad.sock;
        include uwsgi_params;
        uwsgi_read_timeout 300;
    }

    location /static {
	alias /home/desarrollorb/www/discapacidad_migracion/collected;
    }

    access_log /var/log/nginx/discapacidad_access.log;
    error_log /var/log/nginx/discapacidad_error.log;
}

```

* Ajustar permisos y crear el enlace simbolico de sites-enabled hacia sites-available del archivo sisben.conf
    * ``` sudo ln -s /etc/nginx/sites-available/discapacidad.conf /etc/nginx/sites-enabled/discapacidad.conf ```
    * ``` sudo chgrp www-data /etc/nginx/sites-available/discapacidad.conf```
    * ``` sudo chmod 774 /etc/nginx/sites-available/discapacidad.conf```


* Comprobar que la configuración de nginx esté correcta
    * ``` sudo nginx -t ```
* Reiniciar el servidor para que tome el nuevo sitio en caso de que ya haya sido iniciado

#### Iniciar o reiniciar el servidor nginx:
    * ``` sudo service nginx start     # inicia el servidor ```
    * ``` sudo service nginx restart   # reiniciar el servidor ```
    
#### Probar backend
* Para el servidor de pruebas: digitar en la terminal:
    - ``` wget -O- http://discapacidad.qa.aws.rbsas.co:8000 ```
* Para el servidor de producción: digitar en la terminal:
    - ``` wget -O- http://discapacidad.aws.rbsas.co:8000 ```
    
* Si funciona correctamente en ambos casos deberá deberá aparecer el código HTML de la página de inicio.
